#!C:\Python27\python.exe
# Filename: Crypto.py
# -*- coding: utf-8 -*-

import os
import platform
import re
import sys
#from Crypto.Cipher import AES # CBC AES crypto lib
import struct
import hashlib
import random

def Check_File_Exists(filename, Debug=False):
    ''' Input path+filename, returns boolean '''
    if os.path.isfile(filename):
        return True
    else:
        if Debug == True:
            print '[+] File does not exist'
        return False

def Encrypt_File(Key, In_File, Out_File=None, Nonce=None, Chunk_Size=64*1024):
    ''' Encrypts In_File using AES256 in CBC mode.
    In/out files should be full path+file.
    Key is encryption key string; length should be 16 or 32.
    Key is hashed with SHA256 to meet length requirements.
    Nonce is one time number string (IV); length should be 16. 
    First 16 bytes of file will be Nonce '''
    
    if not Out_File:
        Out_File = In_File + '.seamonster'
        
    Hashed_Key = hashlib.sha256(Key).digest()

    Nonce = ''.join(chr(random.randint(0, 0xFF)) for i in range(16))
    Encrypt = AES.new(Hashed_Key, AES.MODE_CBC, Nonce)
    File_Size = os.path.getsize(In_File)

    with open(In_File, 'rb') as infile:
        with open(Out_File, 'wb') as outfile:
            outfile.write(struct.pack('<Q', File_Size))
            outfile.write(Nonce)
            while True:
                Chunk = infile.read(Chunk_Size)
                if len(Chunk) == 0:
                    break
                elif len(Chunk) % 16 != 0:
                    Chunk += ' ' * (16 - len(Chunk) % 16)

                outfile.write(Encrypt.encrypt(Chunk))       
    return Out_File

def Decrypt_File(Key, In_File, Out_File=None, Nonce=None, Chunk_Size=24*1024):
    ''' Decrypts In_File using AES in CBC mode.
    In/out files should be full path+file.
    Key is encryption key string; length should be 16 or 32.
    Key is hashed with SHA256 to meet length requirements.
    Nonce is one time number string (IV); length should be 16. 
    First 16 bytes of file will be Nonce '''
    
    if not Out_File:
        Out_File = os.path.splitext(In_File)[0] # Remove MIME from file
    
    Hashed_Key = hashlib.sha256(Key).digest()
    
    with open(In_File, 'rb') as infile:
        Orig_Size = struct.unpack('<Q', infile.read(struct.calcsize('Q')))[0]
        Nonce = infile.read(16)
        Decrypt = AES.new(Hashed_Key, AES.MODE_CBC, Nonce)
    
        with open(Out_File, 'wb') as outfile:
            while True:
                Chunk = infile.read(Chunk_Size)
                if len(Chunk) == 0:
                    break
                outfile.write(Decrypt.decrypt(Chunk))
    
            outfile.truncate(Orig_Size)
    return Out_File

def Get_String_MD5(String=''):
    ''' Return MD5 checksum of string '''
    MD5_Hasher = hashlib.md5()
    MD5_Hasher.update(String)
    return MD5_Hasher.hexdigest()

def Get_String_SHA256(String=''):
    ''' Return SHA256 checksum of string '''
    SHA256_Hasher = hashlib.sha256()
    SHA256_Hasher.update(String)
    return SHA256_Hasher.hexdigest()

def Get_File_MD5(In_File, Blocksize=65536, Debug=False):
    ''' Return MD5 of file '''
    MD5_Hasher = hashlib.md5()
    with open(In_File, 'rb') as File:
        Buffer = File.read(Blocksize)
        while len(Buffer) > 0:
            MD5_Hasher.update(Buffer)
            Buffer = File.read(Blocksize)
    MD5_Hash = MD5_Hasher.hexdigest()
    if Debug == True:
        print MD5_Hash
    return MD5_Hash

def Get_File_SHA256(In_File, Blocksize=65536, Debug=False):
    ''' Return SHA256 of file '''
    SHA256_Hasher = hashlib.sha256()
    with open(In_File, 'rb') as File:
        Buffer = File.read(Blocksize)
        while len(Buffer) > 0:
            SHA256_Hasher.update(Buffer)
            Buffer = File.read(Blocksize)
    SHA256_Hash = SHA256_Hasher.hexdigest()
    if Debug == True:
        print SHA256_Hash
    return SHA256_Hash

def Get_File_Size(In_File):
    ''' Returns file size in bytes.
    In_File is path+filename '''
    return os.path.getsize(In_File)
