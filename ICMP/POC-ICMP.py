#!/usr/bin/python
# Filename: POC-ICMP.py
# -*- coding: utf-8 -*-

import subprocess
import re
import threading
import datetime

Regex_ICMP_Reply = 'Reply from (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
Regex_IP = '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

def Ping(IP, Debug=False):
    ''' Send ICMP to IPv4 address, return [Boolean, IP, 'stdout from shell'] '''
    Successful = False
    if Debug == True:
        print '\n[*] Pinging:', IP
    Ping_IP = subprocess.Popen(['ping', str(IP), '-n', '1', '-w', '100'],\
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    Ping_Out_IP, Ping_Err_IP = Ping_IP.communicate() # Stdout results to variable
    try:
        Ping_Out_IP = re.findall(Regex_ICMP_Reply, Ping_Out_IP)[0]
    except:
        Ping_Out_IP = ''
        if Debug == True:
            print '[!] Host down:', str(IP) # Debugging
    if len(str(Ping_Out_IP)) > 1:
        if Debug == True:
            print '[+] Host up:', str(IP)
        Successful = True
    return [Successful, IP, Ping_Out_IP]

def Threaded_Ping_List(Host_List=[], Debug=False):
    ''' Send ICMP to a list of IPv4 address, return [Live_Hosts] '''
    Thread_1_List = Host_List[0:len(Host_List) / 3]
    Thread_2_List = Host_List[len(Host_List) / 3:(len(Host_List) / 3)*2]
    Thread_3_List = Host_List[(len(Host_List) / 3)*2::]
    Threaded_Ping(Thread_1_List, Thread_2_List, Thread_3_List)
    
def Threaded_Ping(List_1_Thread, List_2_Thread, List_3_Thread, Debug=True):
    ''' Starts the ping functions as a thread '''
    if Debug == True:
        print '[+] Threaded_Ping running 3 threads'
        print '[+] Total of %d hosts to ping' % int(len(List_1_Thread)+len(List_2_Thread)+len(List_3_Thread))
    #import pdb;pdb.set_trace()
    Thread(target = Ping_Thread_1(List_1_Thread)).start()
    Thread(target = Ping_Thread_2(List_2_Thread)).start()
    Thread(target = Ping_Thread_3(List_3_Thread)).start()

def Ping_Thread_1(Host_List=[], Debug=True):
    ''' THREADED: Send ICMP to a list of IPv4 address, return [Live_Hosts] '''
    global Live_Hosts_Thread_1
    if Debug == True:
        print '[+] Starting thread 1 against %d hosts' % len(Host_List)
    Start_Time = datetime.datetime.now()
    Live_Hosts_Thread_1 = []
    Live_Hosts = []
    for Host in Host_List:
        Result = Ping(Host)
        if Result[0] == True:
            Live_Hosts.append(Host)
    End_Time = datetime.datetime.now()
    Difference = End_Time - Start_Time
    Time_Difference = divmod(Difference.days * 86400 + Difference.seconds, 60)
    if Debug == True:
        print '[+] Thread 1: Got %d live hosts out of %d checked' % (len(Live_Hosts), len(Host_List))
        print '[+] Thread 1: took %s seconds' % str(Time_Difference)
    Live_Hosts_Thread_1 = Live_Hosts
    return Live_Hosts

def Ping_Thread_2(Host_List=[], Debug=True):
    ''' THREADED: Send ICMP to a list of IPv4 address, return [Live_Hosts] '''
    global Live_Hosts_Thread_2
    if Debug == True:
        print '[+] Starting thread 2 against %d hosts' % len(Host_List)
    Start_Time = datetime.datetime.now()
    Live_Hosts_Thread_2 = []
    Live_Hosts = []
    for Host in Host_List:
        Result = Ping(Host)
        if Result[0] == True:
            Live_Hosts.append(Host)
    End_Time = datetime.datetime.now()
    Difference = End_Time - Start_Time
    Time_Difference = divmod(Difference.days * 86400 + Difference.seconds, 60)
    if Debug == True:
        print '[+] Thread 2: Got %d live hosts out of %d checked' % (len(Live_Hosts), len(Host_List))
        print '[+] Thread 2: took %s seconds' % str(Time_Difference)
    Live_Hosts_Thread_2 = Live_Hosts
    return Live_Hosts

def Ping_Thread_3(Host_List=[], Debug=True):
    ''' THREADED: Send ICMP to a list of IPv4 address, return [Live_Hosts] '''
    global Live_Hosts_Thread_3
    if Debug == True:
        print '[+] Starting thread 3 against %d hosts' % len(Host_List)
    Start_Time = datetime.datetime.now()
    Live_Hosts_Thread_3 = []
    Live_Hosts = []
    for Host in Host_List:
        Result = Ping(Host)
        if Result[0] == True:
            Live_Hosts.append(Host)
    End_Time = datetime.datetime.now()
    Difference = End_Time - Start_Time
    Time_Difference = divmod(Difference.days * 86400 + Difference.seconds, 60)
    if Debug == True:
        print '[+] Thread 3: Got %d live hosts out of %d checked' % (len(Live_Hosts), len(Host_List))
        print '[+] Thread 3: took %s seconds' % str(Time_Difference)
    Live_Hosts_Thread_3 = Live_Hosts
    return Live_Hosts

Sample_IPs = ['10.100.11.10', '10.100.11.11', '10.100.11.12',
              '10.100.11.13', '10.100.11.14', '10.100.11.15']

Sample_IPs2 = ['10.100.11.16', '10.100.11.17', '10.100.11.18',
              '10.100.11.19', '10.100.11.20', '10.100.11.21',
              '10.100.11.22', '10.100.11.23', '10.100.11.24',
              '10.100.11.25', '10.100.11.26', '10.100.11.27',
              '10.100.11.28', '10.100.11.29', '10.100.11.30',
              '10.100.11.31', '10.100.11.32', '10.100.11.33',
              '10.100.11.34', '10.100.11.35', '10.100.11.36',
              '10.100.11.37', '10.100.11.38', '10.100.11.39',
              '10.100.11.40', '10.100.11.41', '10.100.11.42',
              '10.100.11.43', '10.100.11.44', '10.100.11.45',
              '10.100.11.46', '10.100.11.47', '10.100.11.48',
              '10.100.11.49', '10.100.11.50', '10.100.11.51',
              '10.100.11.52', '10.100.11.53', '10.100.11.54',
              '10.100.11.55', '10.100.11.56', '10.100.11.57',
              '10.100.11.58', '10.100.11.59', '10.100.11.60']

# Threaded_Ping_List(Sample_IPs2, True)
