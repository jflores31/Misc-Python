#!/usr/bin/env python
# Filename: Python_Server.py
# -*- coding: utf-8 -*-
import socket
import sys

def Start_Server():
	''' Create socket & start listening server '''
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	server_address = ('192.168.1.1', 10000)
	print >>sys.stderr, '\n[+] starting up on %s port %s' % server_address
	sock.bind(server_address)
	sock.listen(1)
	while True:
		print >>sys.stderr, '[+] waiting for connection'
		connection, client_address = sock.accept()
		try:
			print >>sys.stderr, '[+] connection from', client_address
			while True:
				data = connection.recv(256)
				print '\n' * 3
				print >>sys.stderr, '[+] received "%s"' % data
				if data:
					print >>sys.stderr, '[+] sending data back to client'
					connection.sendall(data)
					f = open('ee.pem', 'a+')
					f.write(data)
					f.close()
				else:
					print >>sys.stderr, '[+] No more data from', client_address
					break
		finally:
			connection.close()
