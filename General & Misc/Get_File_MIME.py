#!/usr/bin/python
# Filename: Get_File_MIME.py
# -*- coding: utf-8 -*-

PathFile = '/directory/file.ext.csv'

File_MIME = PathFile[::-1].split('.')[0][::-1].upper()

print File_MIME