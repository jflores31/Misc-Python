#!/usr/bin/python
# Filename: BinBot.py
# -*- coding: utf-8 -*-

import binwalk

# Equivalent to: 'binwalk --signature firmware1.bin firmware2.bin'.
# Note the use of 'quiet=True' to suppress normal binwalk output.
for module in binwalk.scan('gxp2160fw.bin',
                           #'01.1a.a1.3.18.flash.pkg',
                           signature=True,
                           quiet=True,
                           extract=True):

    # binwalk.scan returns a module object for every executed
    # module; in this case, there should be only one, which is
    # the signature module.
    print ("%s Results:" % module.name)

    # Each module has a list of result objects, describing
    # the results returned from the executed module.
    for result in module.results:
        print ("\t%s    0x%.8X    %s" % (result.file.name, 
                                         result.offset, 
                                         result.description))



