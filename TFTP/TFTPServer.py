#!C:\Python27\python.exe
# Filename: TFTPServer.py
# -*- coding: utf-8 -*-

import tftpy

def Start_TFTP_Server(Port=69, Directory='', Bound_IP='0.0.0.0',
                      Incriment_Port_On_Fail=False, Debug=False):
    ''' Start TFTP Server '''
    print '\n[+] Starting TFTP server ...'
    print '[+] Port: %d' % Port
    print '[+] TFTP Root: %s' % Directory
    print '[+] Bound IP: %s' % Bound_IP
    
    TFTP_Server = tftpy.TftpServer(Directory)
    TFTP_Server.listen(Bound_IP, Port) 


if __name__ == '__main__':
    TFTP_Root_Directory = raw_input('\n[+] Enter TFTP Root directory\n[+] ')
    try:
        TFTP_Port = int(raw_input('\n[+] Enter port to bind TFTP Service\n[#] '))
        if TFTP_Port > 65535:
            print '\n[!] Bad value for TFTP port - Try again'
            TFTP_Port = int(raw_input('\n[+] Enter port to bind TFTP Service\n[#] '))
    except:
        print '\n[!] Bad value for TFTP port - Try again'
        try:
            TFTP_Port = int(raw_input('\n[+] Enter port to bind TFTP Service\n[#] '))
        except:
            print '\n[!] Bad value for TFTP port - Using port 69'
            TFTP_Port = 69
    
    Start_TFTP_Server(Bound_IP='127.0.0.1', Port=int(TFTP_Port), Directory=TFTP_Root_Directory)