import pyglet
from pyglet.window import mouse

window = pyglet.window.Window()

global Mouse_Down
Mouse_Down = False

global Distance_X_
Distance_X_ = 0

global Distance_Y_
Distance_Y_ = 0

# Alpha transparency for primitives
pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)

@window.event
def on_draw():
    pass#window.clear()

@window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):
    global Mouse_Down
    global Distance_X_
    global Distance_Y_

    if Mouse_Down == True:
        # Draw temporary quad

        #window.clear()
        pyglet.graphics.glClear(pyglet.graphics.GL_COLOR_BUFFER_BIT) # Clear color buffer

        Distance_X_ += dx
        Distance_Y_ += dy

        """
        Quad_ = ('v2f', (
                         Pressed_[0],
                         Pressed_[1],
                         Pressed_[0] + abs( abs(Pressed_[0]) - abs(x) ),
                         Pressed_[1],
                         Pressed_[0] + abs( abs(Pressed_[0]) - abs(x) ),
                         Pressed_[1] + abs( abs(Pressed_[1]) - abs(y) ),
                         Pressed_[0],
                         Pressed_[1] + abs( abs(Pressed_[1]) - abs(y) )
                        )
                )
        """

        Quad_ = ('v2f', (
                         Pressed_[0],
                         Pressed_[1],
                         Pressed_[0] + Distance_X_,
                         Pressed_[1],
                         Pressed_[0] + Distance_X_,
                         Pressed_[1] + Distance_Y_,
                         Pressed_[0],
                         Pressed_[1] + Distance_Y_,
                        )
                )

        Color_Opacity_ = ('c4B', (217, 52, 52, 53, # RGBA
                                  217, 52, 52, 53, # RGBA
                                  217, 52, 52, 53, # RGBA
                                  217, 52, 52, 53) # RGBA
                         )

        Rectangle_ = pyglet.graphics.draw(4, pyglet.gl.GL_QUADS, Quad_, Color_Opacity_)


        Line_1 = pyglet.graphics.draw(2,
                                    pyglet.gl.GL_LINES,
                                    ('v2f', ( (Pressed_[0] + Distance_X_), Pressed_[1], x, y) ),
                                    ('c3B', (255, 0, 0, 255, 0, 0)) # 0,0,0 - 0,0,0
                                    )


        Line_2 = pyglet.graphics.draw(2,
                                    pyglet.gl.GL_LINES,
                                    ('v2f', ( Pressed_[0], (Pressed_[1] + Distance_Y_), x, y) ),
                                    ('c3B', (255, 0, 0, 255, 0, 0)) # 0,0,0 - 0,0,0
                                    )


        Line_3 = pyglet.graphics.draw(2,
                                    pyglet.gl.GL_LINES,
                                    ('v2f', ( Pressed_[0], Pressed_[1], (x - Distance_X_), y) ),
                                    ('c3B', (255, 0, 0, 255, 0, 0)) # 0,0,0 - 0,0,0
                                    )

        Line_4 = pyglet.graphics.draw(2,
                                    pyglet.gl.GL_LINES,
                                    ('v2f', ( Pressed_[0], Pressed_[1], x, (y - Distance_Y_)) ),
                                    ('c3B', (255, 0, 0, 255, 0, 0)) # 0,0,0 - 0,0,0
                                    )



    elif Mouse_Down == False:
        pass

@window.event
def on_mouse_press(x, y, button, modifiers):
    global Pressed_
    global Mouse_Down

    window.clear()

    print '\n[+] [Press]', x, y

    Pressed_ = [x, y]
    Mouse_Down = True

@window.event
def on_mouse_release(x, y, button, modifiers):
    global Released_
    global Mouse_Down
    global Distance_X_
    global Distance_Y_

    print '[+] [Release]', x, y

    # 'v2f', [x, y, dx, y, dx, dy, x, dy]

    """
    Quad_ = ('v2f', (
                     Pressed_[0],
                     Pressed_[1],
                     Pressed_[0] + abs( abs(Pressed_[0]) - abs(Released_[0]) ),
                     Pressed_[1],
                     Pressed_[0] + abs( abs(Pressed_[0]) - abs(Released_[0]) ),
                     Pressed_[1] + abs( abs(Pressed_[1]) - abs(Released_[1]) ),
                     Pressed_[0],
                     Pressed_[1] + abs( abs(Pressed_[1]) - abs(Released_[1]) )
                    )
            )
    """

    Quad_ = ('v2f', (
                     Pressed_[0],
                     Pressed_[1],
                     Pressed_[0] + Distance_X_,
                     Pressed_[1],
                     Pressed_[0] + Distance_X_,
                     Pressed_[1] + Distance_Y_,
                     Pressed_[0],
                     Pressed_[1] + Distance_Y_,
                    )
            )

    Color_Opacity_ = ('c4B', (217, 52, 52, 53, # RGBA
                              217, 52, 52, 53, # RGBA
                              217, 52, 52, 53, # RGBA
                              217, 52, 52, 53) # RGBA
                     )

    Rectangle_ = pyglet.graphics.draw(4, pyglet.gl.GL_QUADS, Quad_, Color_Opacity_)

    Released_ = [x, y]
    Distance_X_ = 0
    Distance_Y_ = 0
    Mouse_Down = False

    #window.clear()
    #Rectangle_ = None

if __name__ == '__main__':
    window.clear()
    pyglet.app.run()