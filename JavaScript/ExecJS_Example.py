#!/usr/bin/python
# Filename: ExecJS_Example.py
# -*- coding: utf-8 -*-

import execjs

SubmitLoginInfoJS = execjs.compile('''
function submitLoginInfo() {
	$("#notemsg").html("");

	var authHeader = encodeBase64($("input:radio:checked").attr("value") + ":" + $("input[name='password']").attr("value"));
	$.ajax({
		type: "GET",
		url: "auth.htm?t=" + new Date().toGMTString(),
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Basic " + authHeader);
		},
		success: function(result) {
			document.cookie = "Authorization=Basic " + authHeader;
			window.location = "/index.htm";
		},
		error: function(request,err,e){
			$("#notemsg").html("<span style='color: red'>Invalid password. Please try again.</span>");
			$("input[name='password']").attr("value", "");
			document.login.password.focus();
		},
		complete: function(request, textStatus) {
		}
	});
}
''')

SubmitLoginInfoJS.call('submitLoginInfo')







