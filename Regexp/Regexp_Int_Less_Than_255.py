#!/usr/bin/python
# Filename: Regexp_Int_Less_Than_255.py
# -*- coding: utf-8 -*-

import re

# This will capture integer less than 255
Regexp = '^([0-9]|[1][0-9][0-9]|[2][0-5][0-4]|254)$'
