#$language = "Python"
#$interface = "1.0"


#################################################
''' IMPORTAR PAQUETES. '''
#################################################

import SecureCRT
import sys
import os
import time
import datetime as Date
import random
import SecureCRTP

varPath = os.path.dirname(__file__)
if varPath not in sys.path:
	sys.path.insert(0, varPath)

SecureCRTP.Init(crt)

LOG_DIRECTORY = 'C:\\Documentos\\'

DATE = str(Date.datetime.now()).split(' ')

#.split(' ')[1].replace(':', '-')[0:8]

try:
	if not os.path.exists(LOG_DIRECTORY):
		os.mkdir(LOG_DIRECTORY)
except:
	crt.Dialog.MessageBox('No se puede crear C:\\Documentos\\ carpeta. por favor crea manualmente, ahora date click en aceptar.')
	if not os.path.exists(LOG_DIRECTORY):
		LOG_DIRECTORY = '' # si aun no has creado la carpeta por favor establecer la carpeta xd 

def LaunchViewer(filename):
	''' Opens a file '''
	try:
		os.startfile(filename)
	except AttributeError:
		subproccess.call(['open', filename])

def DCAP(Commands=[], Hostname=None):
	''' Ready to go data capture function '''
	# if input is only string, convert to tuple
	if type(Commands) == str:
		x = Commands
		Commands = []
		Commands.append[x]

	# If Hostname isnt specified the we will
	# use the commands fot the filename string

	if Hostname == None:
		try:
			d = str(Date.datetime.now()).split(' ')[0]
			t = str(Date.datetime.now()).split(' ').replace(':', '-')[0:8]

			Hostname = 'DCAP_'+str(random.randint(1,10000))+'_Date_'+d+'_Time_'+t
		except:
			Hostname = 'DCAP_'

	filename = str(Hostname)+'.txt'

	#crt.Dialog.MessageBox(LOG_DIRECTORY+Filename)

	try:
		File = open(LOG_DIRECTORY+Filename, 'wb+')
	except:
		Name = 'DCAP_'+str(random.randint(1,1000000000))+'.txt'
		File = open(LOG_DIRECTORY+Name, 'wb+')

	for Cmd in Commands:
		strOut = SecureCRTP.Run(str(Cmd))
		File.write('\r\n' * 5)
		File.write('==' * 25)
		File.write('\r\nCommands: '+str(Cmd)+'\r\n')
		File.write('--' * 25)
		File.write('\r\n' * 2)
		File.write(strOut)
		#time.sleep(1)
	
	File.close()
	LaunchViewer(LOG_DIRECTORY)

	return

####################################################
''' SCRIPT RUNTIME '''
####################################################

#Hostname = SecureCRTP.Run('show runn | i hostname')
#Hostname.replace(' ', '')

#for Character in crt(Hostname):
#    if Character in BANNED_CHARACTERS:
#        Character = ''

# crt.Dialog.MessageBox(Hostname)

SecureCRTP.Run('term log 0')
SecureCRTP.Run('conf term')
SecureCRTP.Run('int vlan 10')
SecureCRTP.Run('ip add 192.168.1.4 255.255.255.0')
SecureCRTP.Run('end')
SecureCRTP.Run('wr men')

Commands = ['show runn', 'show vers', 'dir', 'show clock',  'show ip int brief', 'show cdp nie',\
            'show ip route', 'show inter']

DCAF(Commands)

