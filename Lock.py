#!/usr/bin/env python
# Filename: Lock.py
# -*- coding: utf-8 -*-

import os
import sys
import re
import random
import time
import datetime
import platform
import threading
import subprocess
import pyxhook
from pymouse import PyMouse

try:
    from mtTkinter import *
except:
    print '\n[!] mtTkinter import failed - threads are not stable!'
    from Tkinter import *

# sudo apt-get install python-xlib

# Hook mouse
Mouse_Hook = PyMouse()

# Log file
Log_File = 'Lock.log'

# Current state
System_Locked = False
Debug_Mode = False

# Hotkey combinations
Lock_Keys = ['Control_L', 'Control_L', 'grave']
Unlock_Keys = ['Control_L', 'Control_L', 'Tab']
Kill_Lock_Keys = ['Control_L', 'Control_L', 'equal']
Debug_Keys = ['Control_L', 'Control_L', 'minus']

Advanced_Lock_Keys = ['Control_L', '(']
Advanced_Unlock_Keys = ['Control_L', ')']

# Advanced lock/unlock combination
# User may define variable keys to be pressed for unlocking
# Unlock_Pattern = ['1', '2', '3']
Unlock_Pattern = []

Unlock_Password = 'iou'

# Recent key presses (one large string)
Recent_Keys = ''

# Host OS
OS = platform.system()

# Local path file ran from
Run_Path = os.path.dirname(os.path.abspath(__file__))

# Paths
if OS == 'Linux':
    Base_Directory = '/'+'/'.join(Run_Path.split('/')[1:-1])+'/'

elif OS == 'Windows':
    Base_Directory = '..\\'

def OnKeyPress(event):
    global Recent_Keys
    global Debug_Mode
    global System_Locked

    print '\n[+] %s' % event.Key
    #Recent_Keys.append(event.Key)
    Recent_Keys = Recent_Keys + event.Key
    #print '\n', Recent_Keys

    # Check recent keys
    if ''.join(Lock_Keys) in Recent_Keys and System_Locked == False:
        print '\n[+] Locking system'
        System_Locked = True
        Mouse_Hook.move(1, 1)
        Recent_Keys = ''
        Start_Thread(Jail_Mouse)
        #Lock_System()

    elif ''.join(Unlock_Keys) in Recent_Keys and System_Locked == True:
        print '\n[+] Unlocking system'
        System_Locked = False
        Locked_Move_Attempts = 0
        Recent_Keys = ''

    # Key (grave) == `
    #elif event.Ascii == 96:
    elif ''.join(Kill_Lock_Keys) in Recent_Keys:
        #f.close()
        #Recent_Keys = ''
        print '\n[+] Killing Lock.py hook...'
        Keyboard_Hook.cancel()
        sys.exit()


    elif ''.join(Debug_Keys) in Recent_Keys:
        Debug_Mode = True
        # import gui
        # pop gui to alert user debug info


    # Cleanup Recent_Keys
    # If over len(100) and not last key == Control_L
    if len(Recent_Keys) > 100 and not Recent_Keys[::-1][0:9][::-1] == 'Control_L':
        Recent_Keys = ''


def Jail_Mouse():
    global System_Locked
    while System_Locked == True:
        Mouse_Hook.move(1, 1)
        time.sleep(1)

def Lock_System():
    global GUI
    # TK GUI
    GUI = Tk() # CREATE GUI (TKINTER)
    GUI.title('UNAUTHORIZED ACCESS ATTEMPT') # Set window name
    #GUI.geometry('1675x1010') # Set window size
    #=GUI.geometry('3355x1010') # Set window size
    #GUI.resizable(width = FALSE, height = FALSE) # Make window not resizable

    Sceen_Width, Sceen_Height = GUI.winfo_screenwidth(), GUI.winfo_screenheight()
    GUI_Width = Sceen_Width * 0.75
    GUI_Height = Sceen_Height * 0.75

    Offset_X = 0
    Offset_Y = 0

    GUI.overrideredirect(1) # No border
    #GUI.geometry('%dx%d+0+0' % (w, h))
    GUI.geometry('%dx%d+%d+%d' % (Sceen_Width, Sceen_Height, Offset_X, Offset_Y))
    GUI.focus_set()
    GUI.resizable(width = FALSE, height = FALSE) # Make window not resizable
    #GUI.bind('<Escape>', lambda e: e.widget.quit())

    # Label
    #Unlock_Label = Label(GUI, text = 'UNAUTHORIZED ACCESS ATTEMPT', font = 'arial 12')
    #Unlock_Label.place(x = 100, y = 200)

    # Text entry
    #Unlock_var = StringVar()
    #Unlock_var.set('')
    #Unlock_Entry = Entry(GUI, bd = 2, width = 13, textvariable = Unlock_var)
    #Unlock_Entry.place(x = 100, y = 300)

    # TK button
    Unlock_Btn = Button(GUI, text='Unlock',
                        width = 13, height = 3, font = 'arial 12 bold',
                        fg='black', bg='#A4CBEE',
                        bd=5, command = lambda: Unlock_Tk())
    Unlock_Btn.place(x = 100, y = 100)

    # Pop GUI
    GUI.mainloop()



def Unlock_System():
    pass

def Log_Event(Pathfile, Log_String):
    f = open(Pathfile, 'a+')
    f.write('\n[+] Event logged:')
    f.write(Log_String+'\n')
    f.write('[!] Time of event: %s\n' % str(datetime.datetime.now()))
    f.close()

def Start_Thread(Thread):
    ''' Start worker thread '''
    global t
    t = threading.Thread(target=Thread)
    t.start()

def Unlock_Tk():
    global System_Locked
    global GUI

    #print '\n\n', Recent_Keys, '\n\n'

    print '\n[+] Tk Unlock success!'
    System_Locked = False
    GUI.destroy()
    #Log_Event(Pathfile=Log_File, Log_String='[+] User succeeded unlock!')

    """
    #if Unlock_var.get() == Unlock_Password:
    #if 'iou' == Unlock_Password:
    if System_Locked == False:
        print '\n[+] Tk Unlock success!'
        #System_Locked = False
        GUI.destroy()
        #GUI.quit
        Log_Event(Pathfile=Log_File, Log_String='[+] User succeeded unlock!')

    elif System_Locked == True:
        print '\n[!] Bad unlock - Attempt logged'
        Log_Event(Pathfile=Log_File, Log_String='[!] User failed unlock!')
    """


def Kill_Python():
    print '\n[+] killall -9 python'
    Example_Process = subprocess.Popen(['killall', '-9', 'python'],\
                      stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    Process_Output, Process_Errors = Example_Process.communicate()
    #print Process_Output


if __name__ == '__main__':
    # Locked mouse move attempts
    Locked_Move_Attempts = 0

    Keyboard_Hook = pyxhook.HookManager()
    Keyboard_Hook.KeyDown = OnKeyPress # Listen to all keystrokes
    Keyboard_Hook.HookKeyboard() # Hook keyboard
    Keyboard_Hook.start() # Start session


    while 1:
        if System_Locked == True:

            # Check mouse POS
            Current_Mouse_POS = Mouse_Hook.position()
            #print Current_Mouse_POS

            if not Current_Mouse_POS == (1, 1):
                Locked_Move_Attempts += 1

            if Locked_Move_Attempts > 5:
                print '\n[!] Unauthorized user attempting to gain access to system!'
                # Log attempts
                Log_Event(Pathfile=Log_File, Log_String='[!] Unauthorized user attempting to gain access to system!')
                # Take picture
                Locked_Move_Attempts = 0 # Reset attempt counter

                # Do action to let user know that you know

                """
                # TK GUI
                GUI = Tk() # CREATE GUI (TKINTER)
                GUI.title('UNAUTHORIZED ACCESS ATTEMPT') # Set window name
                #GUI.geometry('1675x1010') # Set window size
                #=GUI.geometry('3355x1010') # Set window size
                #GUI.resizable(width = FALSE, height = FALSE) # Make window not resizable

                Sceen_Width, Sceen_Height = GUI.winfo_screenwidth(), GUI.winfo_screenheight()
                GUI_Width = Sceen_Width * 0.75
                GUI_Height = Sceen_Height * 0.75

                Offset_X = 0
                Offset_Y = 100

                GUI.overrideredirect(1)
                #GUI.geometry('%dx%d+0+0' % (w, h))
                GUI.geometry('%dx%d+%d+%d' % (Sceen_Width, GUI_Height, Offset_X, Offset_Y))
                GUI.focus_set()
                GUI.resizable(width = FALSE, height = FALSE) # Make window not resizable
                #GUI.bind('<Escape>', lambda e: e.widget.quit())

                # Label
                Unlock_Label = Label(GUI, text = 'UNAUTHORIZED ACCESS ATTEMPT', font = 'arial 12')
                Unlock_Label.place(x = 100, y = 200)

                # Text entry
                #Unlock_var = StringVar()
                #Unlock_var.set('')
                #Unlock_Entry = Entry(GUI, bd = 2, width = 13, textvariable = Unlock_var)
                #Unlock_Entry.place(x = 100, y = 300)

                # TK button
                Unlock_Btn = Button(GUI, text='Unlock',
                                    width = 13, height = 3, font = 'arial 12 bold',
                                    fg='black', bg='#A4CBEE',
                                    bd=5, command = lambda: Unlock_Tk())
                Unlock_Btn.place(x = 100, y = 400)

                # Pop GUI
                GUI.mainloop()
                """





            if Current_Mouse_POS == (1, 1):
                Locked_Move_Attempts = 0

            elif not Current_Mouse_POS == (1, 1):
                pass#Mouse_Hook.move(1, 1)

        elif System_Locked == False:
            pass

        time.sleep(0.5)

