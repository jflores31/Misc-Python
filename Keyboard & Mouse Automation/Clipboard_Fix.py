#!C:\Python27\python.exe
# Filename: Clipboard_Fix.py
# -*- coding: utf-8 -*-

import win32com.client as comclt
import win32clipboard

Shell = comclt.Dispatch('WScript.Shell')

# set clipboard data
win32clipboard.OpenClipboard()
win32clipboard.EmptyClipboard()
win32clipboard.SetClipboardText('testing 123')
win32clipboard.CloseClipboard()

# get clipboard data
win32clipboard.OpenClipboard()
Data = win32clipboard.GetClipboardData()
win32clipboard.CloseClipboard()

