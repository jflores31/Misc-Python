#!C:\Python27\python.exe
# Filename: Windows_API_Example.py
# -*- coding: utf-8 -*-

import time
import re
import os
import sys
import win32api
import win32con
import win32gui
import win32com.client as comclt
import win32clipboard

def Auto_Click(X=0, Y=0, Clicks=1, Delay=float(.1), Sleep=0,
               Button='LEFT', Debug=False):
    ''' LMB auto click n amounts of times at x delay '''
    time.sleep(Delay)
    for iteration in range(Clicks):
        #RSLEEP = float((random.randrange(1,99,3)%100))
        time.sleep(Sleep)
        #time.sleep(RSLEEP)
        win32api.SetCursorPos((X, Y))
        time.sleep(.09)
        win32api.SetCursorPos((X, Y))
        time.sleep(.01)
        if Button.upper() == 'LEFT':
            win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN,0,0)
            win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP,0,0)
        elif Button.upper() == 'RIGHT':
            win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTDOWN,0,0)
            win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP,0,0)
    print '\n[+] %d clicks completed' % Clicks

if __name__ == '__main__':
    Shell = comclt.Dispatch('WScript.Shell')

    # set clipboard data
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText('testing 123')
    win32clipboard.CloseClipboard()

    # get clipboard data
    win32clipboard.OpenClipboard()
    Data = win32clipboard.GetClipboardData()
    win32clipboard.CloseClipboard()

    Auto_Click(X=500, Y=800, Clicks=1, Delay=float(.1))