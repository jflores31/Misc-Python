#!/usr/bin/python
# Filename: MechanizeSample.py
# -*- coding: utf-8 -*-

'''
Sample of mechanize usage
'''

''' Imports '''
import os
import platform
import mechanize
from requests.auth import HTTPDigestAuth

''' Settings '''
# Current path (where file launched)
Current_Path = os.path.dirname(os.path.abspath(__file__))

# Host OS
OS = platform.system()

'''
Mechanize browser settings

# UA header
headers = {
'User-Agent': 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0'
}

br = mechanize.Browser()
br.set_handle_equiv(True)
br.set_handle_redirect(True) # Enable redirect
br.set_handle_referer(True) # Enable X-fowarded header
br.set_handle_robots(False) # Ignore robots.txt
'''

def Mechanize_Example(IP='', Protocol='HTTP', Encoded_Password='080a1443d2f9629084c1eecf561668b8'):
    br = mechanize.Browser()
    br.set_handle_equiv(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)

    # UA header
    headers = {
    'User-Agent': 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0',
    'referer': 'http://'+str(IP)+'/'
    }

    URL = Protocol+'://'+IP+'/admin/advanced'

    #Payload = {'username':'admin', 'userPwd':'080a1443d2f9629084c1eecf561668b8', 'SubmitButton':'Login'}

    '''
    function enc_value(data)
    {
      var pseed2="";
      var buffer1=data;
      var md5Str2="";
      var Length2 = data.length;
      if (Length2 < 10 )
      {
        buffer1 += "0";
        buffer1 += Length2;
      }else{
        buffer1 += Length2;
      }
      Length2 = Length2 + 2;

      for(var p = 0; p < 127; p++) {
        var tempCount = p % Length2;
        pseed2 += buffer1.substring(tempCount, tempCount+1);
      }
      md5Str2 = hex_md5(pseed2);

      return md5Str2;
    }
    '''

    br.open(URL)

    '''
    <loginform post http://192.168.1.10/admin/advanced application/x-www-form-urlencoded
      <SelectControl(userName=[*user, admin])>
      <PasswordControl(userPwd=)>
      <SubmitControl(submitButton=Login) (readonly)>>
    '''

    br.select_form('loginform')
    br.form['userName'] = ['admin',]
    br.form['userPwd'] = '080a1443d2f9629084c1eecf561668b8'
    br.submit()
    

if __name__ == '__main__':
    pass
            

