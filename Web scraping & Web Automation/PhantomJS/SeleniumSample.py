#!/usr/bin/python
# Filename: SeleniumSample.py
# -*- coding: utf-8 -*-

'''
Sample of Selenium usage using geckodriver

Why Selenium? Full browser automation, handles JS
Why not Selenium? Much heavier than other libraries.
'''

''' Imports '''
import os
import platform
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

''' Settings '''
# Current path (where file launched)
Current_Path = os.path.dirname(os.path.abspath(__file__))

# Host OS
OS = platform.system()

# Geckodriver path
GeckoDriver_Path = '/usr/bin/geckodriver'

# Firefox binary path
Firefox_Path = '/usr/bin/firefox'

Protocol = 'HTTPS'
Example_URL = Protocol+'://reddit.com/r/The_Donald'


''' Functions '''
def Selenium_Open_URL(URL=Example_URL, Debug=False):
    ''' Open URL in browser using Selenium 
    return HTML of page '''
    Caps = DesiredCapabilities.FIREFOX
    Caps['marionette'] = True
    Caps['binary'] = Firefox_Path
    
    Driver = webdriver.Firefox(capabilities=Caps, executable_path=GeckoDriver_Path)
    Driver.set_window_size(1920, 1080)
    Driver.get(URL)

    if Debug == True:
        print Driver.title
    
    Example_URL_HTML = Driver.page_source.encode('ascii','ignore')
    
    return Example_URL_HTML


if __name__ == '__main__':
    Selenium_Open_URL()