from ghost import Ghost, Session
import time

ghost = Ghost()

'''
with ghost.start() as session:
    page, extra_resources = session.open("http://jeanphix.me")
    assert page.http_status == 200 and 'jeanphix' in page.content
'''

USERAGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0"


with ghost.start():
    session = Session(ghost, download_images=False, display=True, user_agent=USERAGENT)
    page, rs = session.open("http://10.100.11.12", timeout=120)

    #assert page.http_status == 200

    
    session.evaluate("""
    document.querySelector('input[name="username"]').value = 'admin';
    document.querySelector('input[name="pwd"]').value = 'admin';
    """)

    session.evaluate("""document.querySelector('input[id="idConfirm"]').click();""",
                 expect_loading=True)

    #ghost.wait_page_loaded()
    #session.wait_for_page_loaded()
    time.sleep(2)
    
    #onclick="gotoPage('/servlet?p=phone-preference&q=load');"
    
    #session.evaluate("""document.querySelector('label[onclick="gotoPage('/servlet?p=phone-preference&q=load');"]').click();""",
    #             expect_loading=True)

    #session.evaluate("""document.querySelector('li[id="phone"]').click();""",
    #             expect_loading=True)

    page, resources = session.evaluate('''onclick="gotoPage('/servlet?p=phone-preference&q=load');"''', expect_loading=True)
    
    '''
    """
    import codecs

    with codecs.open('fb.html', encoding='utf-8', mode='w') as f:
       f.write(session.content)
    """
    '''

    time.sleep(10)
    
    # session.save_cookies('fbookie')
    #session.capture_to(path='fbookie.png')
    
    
    # gracefully clean off to avoid errors
    session.webview.setHtml('')
    session.exit()





