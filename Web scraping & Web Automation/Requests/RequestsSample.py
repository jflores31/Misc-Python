#!/usr/bin/python
# Filename: RequestsSample.py
# -*- coding: utf-8 -*-

'''
Sample of requests library using SSL

Why requests? Low overhead, quick and easy

Note: Requests uses urllib3
'''

''' Imports '''
import os
import platform
import requests
import ssl

# Ignore bad certificates using these imports
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
from requests.auth import HTTPDigestAuth

''' Settings '''
# Current path (where file launched)
Current_Path = os.path.dirname(os.path.abspath(__file__))

# Host OS
OS = platform.system()

Protocol = 'HTTPS'
Example_URL = Protocol+'://gitlab.com/Meganerd.eth

# UA header
headers = {
'User-Agent': 'User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0'
'referer': 'https://reddit.com/r/The_Donald'
}

# BURP Proxy settings for debugging
proxy = {'http': 'http://127.0.0.1:8080'}

''' Functions '''

def Requests_POST_Sample(URL=Example_URL, Payload, Auth_Username, Auth_Password, Debug=False):
    ''' '''
    
    Payload = {'Username':'RonBurgandy', 'Password':'ImTrappedInAGlassCaseOfEmotion!'}

    try: 
        URL_Session = requests.post(URL,
                                    headers=headers,
                                    data=Payload,
                                    #proxies=proxy,
                                    auth=HTTPDigestAuth(Auth_Username, Auth_Password),
                                    verify=False)

        URL_HTML = URL_Session.text.encode('ascii','ignore')
    except:
        return False
        
    if URL_Session.status_code == 401:
        # Auth failed!
        return False
    
    elif URL_Session.status_code == 200:
        # 200 OK!
        return True

def Requests_GET_Sample(URL=Example_URL, Payload, Auth_Username, Auth_Password, Debug=False):
    ''' '''

    try: 
        URL_Session = requests.get(URL,
                                   headers=headers,
                                   data=Payload,
                                   #proxies=proxy,
                                   auth=HTTPDigestAuth(Auth_Username, Auth_Password),
                                   verify=False)
        
        
        URL_HTML = URL_Session.text.encode('ascii','ignore')
    except:
        return False
        
    if URL_Session.status_code == 401:
        # Auth failed!
        return False
    
    elif URL_Session.status_code == 200:
        # 200 OK!
        return Truue

if __name__ == '__main__':
    pass
