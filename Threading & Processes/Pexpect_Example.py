#!/usr/bin/python
# Filename: Pexpect_Example.py
# -*- coding: utf-8 -*-

import pexpect

# Start process
child = pexpect.spawn('xev')

# Check for output
child.expect('KeyPress event', timeout=None)

# Read output by line
Line_1 = child.readline()
Line_2 = child.readline()
Line_3 = child.readline()

# Send to process
child.sendline('')