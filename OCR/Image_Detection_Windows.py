#!C:\Python27\python.exe
# Filename: Image_Detection_Windows.py
# -*- coding: utf-8 -*-

import time
import re
import os
import sys
import win32api
import win32con
import win32gui
import win32com.client as comclt
import win32clipboard
import pyscreenshot as ImageGrab
from PIL import Image
import pyautogui

Images_Directory = r'C:\Images\\'
boundaries = []

def Set_Color_Boundaries():
    print '[+] 5 seconds to move the mouse to color 1'
    time.sleep(5)
    im=ImageGrab.grab()
    
    try:
        Pixel_Color = im.getpixel((win32api.GetCursorPos()[0], win32api.GetCursorPos()[1]))
        print '[+] Set first RBG:', Pixel_Color
        boundaries.append((Pixel_Color[0], Pixel_Color[1], Pixel_Color[2]))
    except:
        print '[!] XY RBG: FAILED\n'

    print '[+] 5 seconds to move the mouse to color 2'
    time.sleep(5)
    im=ImageGrab.grab()
    
    try:
        Pixel_Color = im.getpixel((win32api.GetCursorPos()[0], win32api.GetCursorPos()[1]))
        print '[+] Set second RBG:', Pixel_Color
        boundaries.append((Pixel_Color[0], Pixel_Color[1], Pixel_Color[2]))
    except:

def Get_Mouse_Coordinates_and_RBG(Delay=1, Debug=False):
    ''' Get, return cursor X, Y coordinates & pixel color '''
    #for i in range(10000):
    time.sleep(Delay)
    im=ImageGrab.grab()
    MouseXY = win32api.GetCursorPos()
    RBG = im.getpixel((win32api.GetCursorPos()[0], win32api.GetCursorPos()[1]))
    print '\n[+] Cursor XY: '+str(MouseXY)
    print '[+] XY RBG: '+str(RBG)
    return [MouseXY, RBG]

def Get_RBG_From_Coordinates(X=0, Y=0, Debug=False):
    ''' return RBG from coordinates '''
    im=ImageGrab.grab()
    RBG = im.getpixel((X, Y))
    return RBG

def Auto_Click(X=0, Y=0, Clicks=1, Delay=float(.1), Sleep=0,
               Button='LEFT', Debug=False):
    ''' LMB auto click n amounts of times at x delay '''
    time.sleep(Delay)
    for iteration in range(Clicks):
        #RSLEEP = float((random.randrange(1,99,3)%100))
        time.sleep(Sleep)
        #time.sleep(RSLEEP)
        win32api.SetCursorPos((X, Y))
        time.sleep(.09)
        win32api.SetCursorPos((X, Y))
        time.sleep(.01)
        if Button.upper() == 'LEFT':
            win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN,0,0)
            win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP,0,0)
        elif Button.upper() == 'RIGHT':
            win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTDOWN,0,0)
            win32api.mouse_event(win32con.MOUSEEVENTF_RIGHTUP,0,0)
    print '\n[+] %d clicks completed' % Clicks

if __name__ == '__main__':
    # Let user prep any windows
    time.sleep(4)

    Shell = comclt.Dispatch('WScript.Shell')

    # Pop SAP window
    SAP_Window_Names = ['Display Sales Order:', 'Change Sales Order:'
                        'SAP Easy Access', '\\Remote']

    for Window_Name in SAP_Window_Names:
        SAP_Window = Shell.AppActivate(Window_Name)
        if SAP_Window == True:
            print '[+] Found SAP window as: %s' % Window_Name
            time.sleep(.5)
            break
        elif SAP_Window == False:
            pass

    # Determine SAP window borders / window size
    SAP_Window_Top_Left_Corner = pyautogui.locateOnScreen(Images_Directory+'SAP_Top_Left_Corner.png')
    if not SAP_Window_Top_Left_Corner == None:
        print '[+] Found top left corner of SAP window! %s' % str(SAP_Window_Top_Left_Corner)

    # pyautogui example as alternative
    SAP_Quick_Cut_and_Paste = pyautogui.locateOnScreen(Images_Directory+'SAP_Quick_Cut_and_Paste.png')
    pyautogui.moveTo(SAP_Quick_Cut_and_Paste[0]+2,
                     SAP_Quick_Cut_and_Paste[1]+2,
                     duration=.4)
    pyautogui.click()
    