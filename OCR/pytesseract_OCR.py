#!/usr/bin/python
# Filename: pytesseract_OCR.py
# -*- coding: utf-8 -*-

from PIL import Image
import pytesseract

# Get strings from an image
print(pytesseract.image_to_string(Image.open('1.png')))
